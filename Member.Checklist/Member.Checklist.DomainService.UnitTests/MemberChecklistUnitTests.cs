using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoFakeItEasy;
using Castle.Core.Internal;
using FakeItEasy;
using Framework.Db.Base;
using Member.Checklist.DomainService.Models.DTOs;
using Member.Checklist.DomainService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using NUnit.Framework;

namespace Member.Checklist.DomainService.UnitTests
{
    public class MemberChecklistUnitTests
    {

        private IFixture _fixture;
        private ILogger<MemberChecklistService> _fakeLogger;
        private IDbOperations<MemberChecklist> _fakeDbOperations;

        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture().Customize(new AutoFakeItEasyCustomization());
            _fakeLogger = _fixture.Freeze<Fake<ILogger<MemberChecklistService>>>().FakedObject;
            _fakeDbOperations = _fixture.Freeze<Fake<IDbOperations<MemberChecklist>>>().FakedObject;
        }

        [Test]
        public async Task GetAllMemberChecklists_Success_ReturnsOkList()
        {
            var date = DateTime.Today;

            A.CallTo(() => _fakeDbOperations.GetAllAsync()).Returns(new List<MemberChecklist>(){ new MemberChecklist()
                {
                    ChecklistItems = new List<MemberChecklistItem>(),
                    Id = new ObjectId(),
                    UserName = "Fake",
                    WeddingDate = date
                }

            });

            var sut = _fixture.Create<MemberChecklistService>();
            var result = await sut.GetAllMemberChecklists();

            Assert.That(!result.IsNullOrEmpty());
            var list = result.ToList();

            Assert.That(list.Count.Equals(1));
            Assert.That(list.First().UserName == "Fake");
            Assert.That(list.First().WeddingDate == date);
            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to get all checklists").MustNotHaveHappened();
        }

        [Test]
        public void GetAllMemberChecklists_Failure_ThrowsException()
        {
            var fakeLogger = _fixture.Freeze<ILogger<MemberChecklistService>>();
            var fakeDbOperations = _fixture.Freeze<IDbOperations<MemberChecklist>>();

            var sut = _fixture.Create<MemberChecklistService>();

            A.CallTo(() => fakeDbOperations.GetAllAsync()).ThrowsAsync(new Exception("Baaaah"));

            Assert.ThrowsAsync<Exception>(async () => await sut.GetAllMemberChecklists());

            fakeLogger.VerifyLogHappened(LogLevel.Error, "Failed to get all checklists");
            
        }

        [Test]
        public async Task GetMemberChecklist_Success_ReturnsChecklist()
        {
            var date = DateTime.Today;

            var sut = _fixture.Create<MemberChecklistService>();

            var checklist = new MemberChecklist()
            {
                UserName = "Fake",
                WeddingDate = DateTime.Today
            };

            A.CallTo(() => _fakeDbOperations.GetAsync(A<Expression<Func<MemberChecklist, bool>>>._)).Returns(checklist);

            var result = await sut.GetMemberChecklist("Ahhh");

            A.CallTo(() => _fakeDbOperations.GetAsync(A<Expression<Func<MemberChecklist, bool>>>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to get checklist").MustNotHaveHappened();
            
        }

        [Test]
        public void GetMemberChecklist_Failure_LogsError()
        {
            var date = DateTime.Today;

            var sut = _fixture.Create<MemberChecklistService>();

            A.CallTo(() => _fakeDbOperations.GetAsync(A<Expression<Func<MemberChecklist, bool>>>._)).ThrowsAsync(new Exception("Booooo"));

            Assert.ThrowsAsync<Exception>(async () => await sut.GetMemberChecklist("Ahhh"));

            A.CallTo(() => _fakeDbOperations.GetAsync(A<Expression<Func<MemberChecklist, bool>>>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to get Checklist").MustHaveHappened();
        }

        [Test]
        public async Task CreateMemberChecklist_Success_DoesNotLogError()
        {

            var sut = _fixture.Create<MemberChecklistService>();

            await sut.CreateMemberChecklist(new MemberChecklist());

            A.CallTo(() => _fakeDbOperations.CreateAsync(A<MemberChecklist>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to create Checklist").MustNotHaveHappened();
        }

        [Test]
        public void CreateMemberChecklist_Failure_LogsError()
        {
            A.CallTo(() => _fakeDbOperations.CreateAsync(A<MemberChecklist>._)).ThrowsAsync(new Exception());

            var sut = _fixture.Create<MemberChecklistService>();

            Assert.ThrowsAsync<Exception>(async () => await sut.CreateMemberChecklist(new MemberChecklist()));

            A.CallTo(() => _fakeDbOperations.CreateAsync(A<MemberChecklist>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to create Checklist").MustHaveHappened();
        }

        [Test]
        public async Task UpdateMemberChecklist_Success_DoesNotLogError()
        {

            var sut = _fixture.Create<MemberChecklistService>();

            await sut.UpdateMemberChecklist(new MemberChecklist());

            A.CallTo(() => _fakeDbOperations.UpdateAsync(A<Expression<Func<MemberChecklist, bool>>>._, A<MemberChecklist>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to update Checklist").MustNotHaveHappened();
        }

        [Test]
        public void UpdateMemberChecklist_Failure_LogsError()
        {
            A.CallTo(() => _fakeDbOperations.UpdateAsync(A<Expression<Func<MemberChecklist, bool>>>._, A<MemberChecklist>._)).ThrowsAsync(new Exception());

            var sut = _fixture.Create<MemberChecklistService>();

            Assert.ThrowsAsync<Exception>(async () => await sut.UpdateMemberChecklist(new MemberChecklist()));

            A.CallTo(() => _fakeDbOperations.UpdateAsync(A<Expression<Func<MemberChecklist, bool>>>._, A<MemberChecklist>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to update Checklist").MustHaveHappened();
        }

        [Test]
        public async Task DeleteMemberChecklist_Success_DoesNotLogError()
        {
            var sut = _fixture.Create<MemberChecklistService>();

            await sut.DeleteMemberChecklist("boo");

            A.CallTo(() => _fakeDbOperations.RemoveAsync(A<Expression<Func<MemberChecklist, bool>>>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to delete Checklist").MustNotHaveHappened();
        }

        [Test]
        public void DeleteMemberChecklist_Failure_LogsError()
        {
            A.CallTo(() => _fakeDbOperations.RemoveAsync(A<Expression<Func<MemberChecklist, bool>>>._)).ThrowsAsync(new Exception());

            var sut = _fixture.Create<MemberChecklistService>();

            Assert.ThrowsAsync<Exception>(async () => await sut.DeleteMemberChecklist("boo"));

            A.CallTo(() => _fakeDbOperations.RemoveAsync(A<Expression<Func<MemberChecklist, bool>>>._)).MustHaveHappened();

            _fakeLogger.VerifyLog(LogLevel.Error, "Failed to delete Checklist").MustHaveHappened();
        }




    }
}
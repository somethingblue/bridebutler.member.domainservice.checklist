﻿using System;
using System.Collections.Generic;
using System.Text;
using FakeItEasy;
using FakeItEasy.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Internal;

namespace Member.Checklist.DomainService.UnitTests
{
    public static class LoggerExtensions
    {
        public static void VerifyLogHappened<T>(this ILogger<T> logger, LogLevel level, string message)
        {
            try
            {
                logger.VerifyLog(level, message)
                    .MustHaveHappened();
            }
            catch (Exception e)
            {
                throw new ExpectationException($"while verifying a call to log with message: \"{message}\"", e);
            }
        }

        public static IVoidArgumentValidationConfiguration VerifyLog<T>(this ILogger<T> logger, LogLevel level,
            string message)
        {
            return A.CallTo(() => logger.Log(level, A<EventId>._,
                A<FormattedLogValues>.That.Matches(e => e.ToString().Contains(message)), A<Exception>._,
                A<Func<object, Exception, string>>._));
        }
    }
}

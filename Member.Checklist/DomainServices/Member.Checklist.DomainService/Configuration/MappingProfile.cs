﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Member.Checklist.DomainService.Models.DTOs;
using Member.Checklist.DomainService.Models.Requests;

namespace Member.Checklist.DomainService.Configuration
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateMemberChecklistRequest, MemberChecklist>();
            CreateMap<GetMemberChecklistRequest, MemberChecklist>();
            CreateMap<MemberChecklist, GetMemberChecklistResponse>();
            CreateMap<AddMemberChecklistItemRequest, MemberChecklistItem>();
            CreateMap<UpdateMemberChecklistItemRequest, MemberChecklistItem>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Member.Checklist.DomainService.Models.DTOs;
using Member.Checklist.DomainService.Models.Requests;
using Member.Checklist.DomainService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Member.Checklist.DomainService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberChecklistController : ControllerBase
    {
        private readonly ILogger<MemberChecklistController> _logger;
        private readonly IMemberChecklistService _checklistService;
        private readonly IMapper _mapper;

        public MemberChecklistController(ILogger<MemberChecklistController> logger, IMemberChecklistService checklistService, IMapper mapper)
        {
            _logger = logger;
            _checklistService = checklistService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult> CreateMemberChecklist([FromBody]CreateMemberChecklistRequest request)
        {

            var checklist = _mapper.Map<MemberChecklist>(request);
                await _checklistService.CreateMemberChecklist(checklist).ConfigureAwait(false);

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult<GetMemberChecklistResponse>> GetMemberChecklist(
            [FromQuery] GetMemberChecklistRequest request)
        {
            var result = await _checklistService.GetMemberChecklist(request.UserName).ConfigureAwait(false);

            var shit = _mapper.Map<GetMemberChecklistResponse>(result);

            return Ok(_mapper.Map<GetMemberChecklistResponse>(result));
        }

        [HttpPut]
        public async Task<ActionResult> UpdateMemberChecklist([FromBody] CreateMemberChecklistRequest request)
        {
            var checklist = _mapper.Map<MemberChecklist>(request);
            await _checklistService.UpdateMemberChecklist(checklist);

            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteMemberChecklist()
        {
            await _checklistService.DeleteAll();
            return Ok();
        }
    }
}
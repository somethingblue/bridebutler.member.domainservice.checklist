﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Member.Checklist.DomainService.Models.DTOs;
using Member.Checklist.DomainService.Models.Requests;
using Member.Checklist.DomainService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Member.Checklist.DomainService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberChecklistItemController : ControllerBase
    {
        private readonly ILogger<MemberChecklistItemController> _logger;
        private readonly IMemberChecklistItemService _checklistItemService;
        private readonly IMapper _mapper;

        public  MemberChecklistItemController(ILogger<MemberChecklistItemController> logger, IMemberChecklistItemService checklistItemService, IMapper mapper)
        {
            _logger = logger;
            _checklistItemService = checklistItemService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult> AddMemberChecklistItem(AddMemberChecklistItemRequest request)
        {
            try
            {
                await _checklistItemService.AddMemberChecklistItem(request.UserName, _mapper.Map<MemberChecklistItem>(request));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to add member checklist item", request);
                return StatusCode(500);
            }

            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> UpdateMemberChecklistItem(UpdateMemberChecklistItemRequest request)
        {
            try
            {
                await _checklistItemService.UpdateMemberChecklistItem(request.UserName, _mapper.Map<MemberChecklistItem>(request));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to update member checklist item", request);
                return StatusCode(500);
            }

            return Ok();
        }

    }
}
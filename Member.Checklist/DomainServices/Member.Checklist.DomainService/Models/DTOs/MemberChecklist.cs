using System;
using System.Collections.Generic;
using Framework.Db.Mongo;

namespace Member.Checklist.DomainService.Models.DTOs
{
    public class MemberChecklist : MongoEntry
    {
        public string UserName { get; set; }

        public IEnumerable<MemberChecklistItem> ChecklistItems = new List<MemberChecklistItem>();

        public DateTime WeddingDate { get; set; }
    }
}

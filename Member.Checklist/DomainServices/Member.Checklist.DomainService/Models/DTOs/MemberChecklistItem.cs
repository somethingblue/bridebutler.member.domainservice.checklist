using System;
using Framework.Db.Mongo;

namespace Member.Checklist.DomainService.Models.DTOs
{
    public class MemberChecklistItem : MongoEntry
    {

        public string Name { get; set; }

        public string VendorType { get; set; }
         
        public MemberChecklistItemTemplate ChecklistItemTemplate { get; set; }

        public DateTime DueDate { get; set; }

    }
}

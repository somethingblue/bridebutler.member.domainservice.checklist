﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Member.Checklist.DomainService.Models
{
    public enum VendorType
    {
        Photographer, Florist, Venue, Baker, Officiant, Musical, Videographer, Caterer, Dress, Invitations
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Member.Checklist.DomainService.Models.DTOs;

namespace Member.Checklist.DomainService.Models.Requests
{
    public class AddMemberChecklistItemRequest
    {
        public string UserName { get; set; }
        public string Name { get; set; }

        public string VendorType { get; set; }

        public MemberChecklistItemTemplate ChecklistItemTemplate { get; set; }

        public DateTime DueDate { get; set; }
    }
}

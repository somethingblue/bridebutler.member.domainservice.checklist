﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Member.Checklist.DomainService.Models.Requests
{
    public class CreateMemberChecklistRequest
    {
        public string UserName { get; set; }

        public DateTime WeddingDate { get; set; }
    }

}

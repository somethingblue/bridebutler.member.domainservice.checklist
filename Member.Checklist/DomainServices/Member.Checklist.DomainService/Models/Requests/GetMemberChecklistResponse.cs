﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Member.Checklist.DomainService.Models.DTOs;

namespace Member.Checklist.DomainService.Models.Requests
{
    public class GetMemberChecklistResponse
    {
        public string UserName { get; set; }

        public IEnumerable<MemberChecklistItem> ChecklistItems { get; set; }

        public DateTime WeddingDate { get; set; }

    }
}

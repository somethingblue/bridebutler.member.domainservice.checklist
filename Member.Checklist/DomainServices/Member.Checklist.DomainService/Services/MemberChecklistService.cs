﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Db.Base;
using Framework.Db.Mongo;
using Member.Checklist.DomainService.Models.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Member.Checklist.DomainService.Services
{
    public interface IMemberChecklistService
    {
        Task<IEnumerable<MemberChecklist>> GetAllMemberChecklists();
        Task<MemberChecklist> GetMemberChecklist(string userName);
        Task CreateMemberChecklist(MemberChecklist checklist);
        Task UpdateMemberChecklist(MemberChecklist updatedChecklist);
        Task DeleteMemberChecklist(string userName);
        Task DeleteAll();
    }


    public class MemberChecklistService : IMemberChecklistService
    {

        private readonly IDbOperations<MemberChecklist> _memberDbOperations;
        private readonly ILogger<MemberChecklistService> _logger;

        public MemberChecklistService(IOptions<MemberChecklistConfig> config, ILogger<MemberChecklistService> logger, IDbOperations<MemberChecklist> memberDbOperations)
        {
            _memberDbOperations = memberDbOperations;
            _memberDbOperations.InitializeDb(config.Value.ConnectionString, config.Value.DatabaseName, config.Value.CollectionName);
            _logger = logger;
        }

        public async Task<IEnumerable<MemberChecklist>> GetAllMemberChecklists()
        {
            try
            {
                var result = await _memberDbOperations.GetAllAsync();
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to get all checklists");
                throw;
            }
        }

        public async Task<MemberChecklist> GetMemberChecklist(string userName)
        {
            try
            {
                return await _memberDbOperations.GetAsync(e => e.UserName == userName);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to get Checklist", new { userName = userName });
                throw;
            }
        }

        public async Task CreateMemberChecklist(MemberChecklist checklist)
        {
            try
            {
                await _memberDbOperations.CreateAsync(checklist);
            }
            catch (Exception e)
            {
                _logger.LogError(e,"Failed to create Checklist", checklist);
                throw;
            }
        }

        public async Task UpdateMemberChecklist(MemberChecklist checklist)
        {
            try
            {
                await _memberDbOperations.UpdateAsync(e => e.UserName == checklist.UserName, checklist);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to update Checklist", checklist);
                throw;
            }
        }

        public async Task DeleteMemberChecklist(string userName)
        {
            try
            {
                await _memberDbOperations.RemoveAsync(e => e.UserName == userName);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to delete Checklist", new { userName = userName});
                throw;
            }
        }
    
        public async Task DeleteAll()
        {
            await _memberDbOperations.RemoveAllAsync();
        }
    }
}

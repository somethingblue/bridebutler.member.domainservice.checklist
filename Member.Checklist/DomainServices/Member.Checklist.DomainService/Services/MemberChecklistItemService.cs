﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Framework.Db.Base;
using Framework.Db.Mongo;
using Member.Checklist.DomainService.Models.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Member.Checklist.DomainService.Services
{
    public interface IMemberChecklistItemService
    {
        Task AddMemberChecklistItem(string userName, MemberChecklistItem item);
        Task AddDefaultChecklistItems(string userName);
        Task UpdateMemberChecklistItem(string userName, MemberChecklistItem updateItem);
    }

    public class MemberChecklistItemService : IMemberChecklistItemService
    {
        private readonly ILogger<MemberChecklistItemService> _logger;
        private readonly IMemberChecklistService _checklistService;
        private readonly IDbOperations<MemberChecklist> _memberDbOperations;

        public MemberChecklistItemService(ILogger<MemberChecklistItemService> logger, IOptions<MemberChecklistConfig> config, IMemberChecklistService checklistService)
        {
            _logger = logger;
            _checklistService = checklistService;
            _memberDbOperations = new MongoOperations<MemberChecklist>();
            _memberDbOperations.InitializeDb(config.Value.ConnectionString, config.Value.DatabaseName, config.Value.CollectionName);
            
        }

        public async Task AddMemberChecklistItem(string userName, MemberChecklistItem item)
        {

            var checklist = await _checklistService.GetMemberChecklist(userName).ConfigureAwait(false);

            if (checklist == null)
            {
                _logger.LogError("Failed to find member checklist for user", new { Username = userName });
                throw new DataException("Failed to find member checklist for user");
            }

            checklist.ChecklistItems = checklist.ChecklistItems.Append(item);

            try
            {
                await _memberDbOperations.UpdateAsync(e => e.UserName == userName, checklist);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to add item to checklist", checklist);
                throw;
            }

        }

        public async Task AddDefaultChecklistItems(string userName)
        {
        }

        public async Task UpdateMemberChecklistItem(string userName, MemberChecklistItem updateItem)
        {
            var checklist = await _checklistService.GetMemberChecklist(userName).ConfigureAwait(false);

            if (checklist == null)
            {
                _logger.LogError("Failed to find member checklist for user", new { Username = userName });
                throw new DataException("Failed to find member checklist for user");
            }

            var updatedItems = checklist.ChecklistItems.Select(e => e.Name == updateItem.Name ? updateItem : e);

            checklist.ChecklistItems = updatedItems;

            try
            {
                await _memberDbOperations.UpdateAsync(e => e.UserName == userName, checklist);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to update item in checklist", checklist);
                throw;
            }
        }

    }
}
